package com.example.analizsampleapp;

import android.app.Application;

import getpoi.com.poibeaconsdk.PoiScanner;

public class AplicationClass extends Application {

    private String appId="appId";
    private String secretId="secretId";
    private String uniqueId="uniqueID";

    @Override
    public void onCreate() {
        super.onCreate();


        try {
            PoiScanner.Config config = new PoiScanner.Config(secretId,uniqueId,appId);
            config.setEnabled(true);
            config.setOpenSystemBluetooth(false);

            PoiScanner.init(config,this);
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

}