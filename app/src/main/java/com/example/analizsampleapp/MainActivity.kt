package com.example.analizsampleapp

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import getpoi.com.poibeaconsdk.PoiScanner
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"

    private val REQUEST_FOREGROUND_AND_BACKGROUND_REQUEST_CODE = 56
    private val REQUEST_COARSE_LOCATION = 57

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


            askLocalPermission()

        var button2 = findViewById<Button>(R.id.button2)
        var button3 = findViewById<Button>(R.id.button3)

        button2.setOnClickListener {
            startScan()
        }
        button3.setOnClickListener {
            stopScan()
        }

    }

    override fun onResume() {
        super.onResume()
        PoiScanner.bind(object : PoiScanner.PoiResponseListener {
            override fun onResponse(p0: String?) {
                Log.i(TAG,"detected beacon : $p0")
            }

            override fun onFail(p0: Exception?) {
                Log.i(TAG,"Fail : $p0")

            }

        })
    }


    private fun askLocalPermission() {
        if (Build.VERSION.SDK_INT >= 29) {
            val hasFineLocationPerm = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            val hasBackgroundLocationPerm = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            if (hasFineLocationPerm != PackageManager.PERMISSION_GRANTED && hasBackgroundLocationPerm != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION),
                        REQUEST_FOREGROUND_AND_BACKGROUND_REQUEST_CODE)
            }
        } else {
            val hasCourseLocation = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            if (hasCourseLocation != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_COARSE_LOCATION)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_FOREGROUND_AND_BACKGROUND_REQUEST_CODE) {
            if (PackageManager.PERMISSION_GRANTED == grantResults[0] || PackageManager.PERMISSION_GRANTED == grantResults[1]) {
                Log.i(TAG, "Foreground OR background location enabled.")

            } else {
                Log.e(TAG, "Permission was denied, but is needed for core functionality.")
            }
        } else if (requestCode == REQUEST_COARSE_LOCATION) {
            if (PackageManager.PERMISSION_GRANTED == grantResults[0]) {

            } else {
                Log.e(TAG, "Permission was denied, but is needed for core functionality.")
            }
        }
    }

    private fun startScan() {
        PoiScanner.startScan(this)
    }

    private fun stopScan() {
        PoiScanner.stopScan(this)
    }
}
